import json

import numpy
import pandas as pd
from scipy import stats
from sklearn import mixture


def detect_cluster_number_aic(ar):
    best_number_of_components = 1
    best_AIC = 0

    for i in range(1, int(ar.shape[0] / 5)):
        EM = mixture.GaussianMixture(n_components=i, covariance_type='full', max_iter=100);
        EM.fit(ar)

        likelihood = EM.score(ar)
        likelihood_new = (likelihood.sum())
        k = i * (ar.shape[1] + int((ar.shape[1] * (ar.shape[1] + 1)) / 2) + 1)
        print(k, likelihood_new, i, ar.shape[1])

        AIC = 2 * k - 2 * likelihood_new

        if (AIC < best_AIC or best_AIC == 0):
            best_number_of_components = i
            best_AIC = AIC

        if (i - best_number_of_components > 10):
            return best_number_of_components


def detect_cluster_number(ar):
    likelihood = 0
    best_number_of_components = 1
    best_likelihood = 1

    for i in range(1, int(ar.shape[0] / 5)):
        EM = mixture.GaussianMixture(n_components=i, covariance_type='full', max_iter=100);
        EM.fit(ar)

        if (i > 1):
            likelihood = EM.score(ar)
            like_ratio = 2 * (likelihood.sum() - best_likelihood.sum())

            df = (i - best_number_of_components) * (
            ar.shape[1] + int((ar.shape[1] * (ar.shape[1] - 1)) / 2) + ar.shape[1] + 1)
            chi2_p_value = stats.chi2.sf(like_ratio, df)

            if (chi2_p_value < 0.05):
                best_number_of_components = i
                best_likelihood = likelihood

            if (i - best_number_of_components > 10):
                return best_number_of_components
        else:
            best_likelihood = EM.score(ar)

    return best_number_of_components
